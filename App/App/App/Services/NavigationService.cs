﻿using App.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Services
{
    public class NavigationService
    {


        #region Methods
        public void SetMainPage(string pageName)
        {
            switch (pageName)
            {

                case "MasterPage":
                    App.Current.MainPage = new MasterPage();
                    break;

                case "LoginPage":
                    App.Current.MainPage = new LoginPage();
                    break;
                case "ForgotPasswordPage":
                    App.Current.MainPage = new ForgotPasswordPage();
                    break;
                case "RegisterPage":
                    App.Current.MainPage = new RegisterPage();
                    break;
                





            }
        }

        public async Task BackOnMaster()
        {
            await App.Navigator.PopAsync();
        }

        public async Task Navigate(string pageName)
        {

            App.Master.IsPresented = false;
            switch (pageName)
            {


                case "MasterPage":
                    await App.Navigator.PushAsync(new MasterPage());
                    break;

                case "LoginPage":
                    await App.Navigator.PushAsync(new LoginPage());
                    break;
               
                case "ChangePasswordPage":
                    await App.Navigator.PushAsync(new ChangePasswordPage());
                    break;
                case "DetailEmployeePage":
                    await App.Navigator.PushAsync(new DetailEmployeePage());
                    break;
                case "CreatePage":
                    await App.Navigator.PushAsync(new CreatePage());
                    break;
                case "DetailsPage":
                    await App.Navigator.PushAsync(new DetailsPage());
                    break;
                case "EditPage":
                    await App.Navigator.PushAsync(new EditPage());
                    break;
                    







                default:
                    break;
            }
        }

        public async Task Back()
        {
            await App.Navigator.PopAsync();
        }
        #endregion
    }
}
