﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Xamarin.Forms;
    using Helpers;
    using Services;
    using ViewModels;

    public partial class Employee
    {
        public NavigationService navigationService { get; set; }
        #region  Properties
        [JsonProperty("EmployeeId")]
        public int EmployeeId { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("Picture")]
        public string Picture { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

      

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        public string FullPicture
        {
            get
            {
                if (string.IsNullOrEmpty(Picture))
                {
                    return "no_image.png";
                }

                else
                {
                    return string.Format("http://backendservicespro.azurewebsites.net{0}", Picture.Substring(1));
                }




            }
        }

        public override int GetHashCode()
        {
            return EmployeeId;
        }

        #endregion
        #region Commands

        public Employee()
        {
            navigationService = new NavigationService();
        }

        public ICommand SelectEmployeeCommand
        {
            get
            {
                return new RelayCommand(SelectEmployee);
            }
        }

        private async void SelectEmployee()
        {

            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.DetailEmployee = new DetailEmployeeViewModel(this);
            mainViewModel.Agenda = new AgendaViewModel(Convert.ToInt32(this.EmployeeId));
            await navigationService.Navigate("DetailEmployeePage");
        }


        #endregion
    }

}
