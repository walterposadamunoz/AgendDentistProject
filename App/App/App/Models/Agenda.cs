﻿using App.Helpers;
using App.Services;
using App.ViewModels;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace App.Models
{
    public class Agenda : BaseViewModel
    {
        #region Services
        private NavigationService navigationService;
        private DialogService dialogService;

        private APIService apiService;
        private DataService dataService;

        #endregion

        #region Properties
        public int AgendaId { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Hour { get; set; }
        public int UserId { get; set; }
        public int EmployeeId { get; set; }
        public string Description { get; set; }
        public string Duration { get; set; }
        private bool isRunning;
        private bool isEnabled;
        public override int GetHashCode()
        {
            return AgendaId;
        }
        #endregion
        #region Properties

        public bool IsRunning
        {
            get { return this.isRunning; }
            set { SetValue(ref this.isRunning, value); }
        }

        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set { SetValue(ref this.isEnabled, value); }
        }
        #endregion
        #region Constructor
        public Agenda()
        {
            apiService = new APIService();
            dataService = new DataService();
            navigationService = new NavigationService();
            dialogService = new DialogService();
        }
        #endregion
        #region Commands
        public ICommand CreateCommand { get { return new RelayCommand(Selected); } }
        
        

        #endregion

        #region Methods
        async void Selected()
        {

            this.IsRunning = true;
            this.IsEnabled = false;

            var checkConnetion = await this.apiService.CheckConnection();
            if (!checkConnetion.IsSuccess)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    checkConnetion.Message,
                    Languages.Accept);
                return;
            }



            var apiSecurity = Application.Current.Resources["APISecurity"].ToString();
            AgendaViewModel.GetInstance().Agendas.Remove(this);
            this.Description = "Ocupado";
            var response = await this.apiService.Post(
                apiSecurity,
                "/api",
                "/Agenda",
                MainViewModel.GetInstance().Token.TokenType,
                MainViewModel.GetInstance().Token.AccessToken,
                this);

            if (!response.IsSuccess)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    response.Message,
                    Languages.Accept);
                return;
            }
            this.IsRunning = false;
            this.IsEnabled = true;
            await dialogService.ShowMessage(
                Languages.Confirmation,
                Languages.Add);
            AgendaViewModel.GetInstance().Add(this);

        }


        public ICommand DeleteCommand
        {
            get
            {
                return new RelayCommand(Delete);
            }
        }

        async void Delete()
        {
            var response = await dialogService.ShowConfirm(
                Languages.Confirmation,
                Languages.Eliminated);
            if (!response)
            {
                return;
            }

            await AgendaViewModel.GetInstance().Delete(this);
        }

        #endregion
    }
}
